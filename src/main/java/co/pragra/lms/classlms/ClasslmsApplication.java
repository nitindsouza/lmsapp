package co.pragra.lms.classlms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClasslmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClasslmsApplication.class, args);
	}

}
