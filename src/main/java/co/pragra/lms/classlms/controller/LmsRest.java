package co.pragra.lms.classlms.controller;

import co.pragra.lms.classlms.services.StudentService;
import co.pragra.lms.resources.models.Student;
import co.pragra.lms.resources.rest.interfaces.StudentsApi;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
public class LmsRest implements StudentsApi {

    private StudentService service;

    public LmsRest(StudentService service) {
        this.service = service;
    }


    @Override
    public ResponseEntity<Student> addStudent(Student body) {
        co.pragra.lms.classlms.model.Student student = new co.pragra.lms.classlms.model.Student();
        student.setName(body.getName());
        student.setCreated_date(new Date());
        co.pragra.lms.classlms.model.Student st = this.service.createStudent(student);
        Student student1 = new Student();
        student1.setId(st.getId());
        student1.setName(st.getName());

        return ResponseEntity.accepted().body(student1);
    }

    @Override
    public ResponseEntity<List<Student>> studentsGet() {
        return null;
    }

    @Override
    public ResponseEntity<Student> studentsIdGet(Integer id) {
        return null;
    }

    @Override
    public ResponseEntity<Student> updateStudent(Student body) {
        return null;
    }
}
