package co.pragra.lms.classlms.repository;

import co.pragra.lms.classlms.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepo extends JpaRepository<Student,Long> {


}
