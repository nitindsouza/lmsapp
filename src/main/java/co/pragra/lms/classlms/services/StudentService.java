package co.pragra.lms.classlms.services;

import co.pragra.lms.classlms.model.Student;
import co.pragra.lms.classlms.repository.StudentRepo;
import com.sun.javaws.exceptions.InvalidArgumentException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {
    private StudentRepo studentRepo;

    public StudentService(StudentRepo studentRepo) {
        this.studentRepo = studentRepo;
    }

    public List<Student> getStudents(){
        return this.studentRepo.findAll();
    }

    public Student getStudent(Long id){
        return this.studentRepo.findById(id).get();
    }

    public Student createStudent(Student student){
        return this.studentRepo.save(student);
    }

    public Student enrollToCourse(Long studentid, Long courseid){
        Optional<Student> student = this.studentRepo.findById(studentid);
        if(!student.isPresent()){
            throw new IllegalArgumentException("No such student exists in Database");
        }
        student.get().getCourses().add(courseid);

        return student.get();
    }
}
